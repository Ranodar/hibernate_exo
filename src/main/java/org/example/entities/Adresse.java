package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "adresse")
public class Adresse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String rue;

    private String ville;

    private String cp;

    @OneToOne
    @JoinColumn(name = "commande")
    private Commande commande;

    //constructors

    public Adresse() {
    }

    public Adresse(String rue, String ville, String cp) {
        this.rue = rue;
        this.ville = ville;
        this.cp = cp;
    }

    //methodes


    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }


    //toString
    @Override
    public String toString() {
        return "Adresse{" +
                "id=" + id +
                ", rue='" + rue + '\'' +
                ", ville='" + ville + '\'' +
                ", cp='" + cp + '\'' +
                ", commande=" + commande +
                '}';
    }
}
