package org.example.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "commande")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany
    @Column(name = "produits")
    private Set<Produit> produits = new HashSet<>();

    private double total;

    private LocalDate date = LocalDate.now();

    @OneToOne
    private Adresse adresse;

    //constructors
    public Commande() {
    }

    public Commande(Set<Produit> produits, double total) {
        this.produits = produits;
        this.total = total;
    }

    //methodes
    public void ajouterProduits(Produit pr){
        produits.add(pr);
    }

    //getters et setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Produit> getProduits() {
        return produits;
    }

    public void setProduits(Set<Produit> produits) {
        this.produits = produits;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    //toString
    @Override
    public String toString() {
        return "Commande{" +
                "id=" + id +
                ", produits=" + produits +
                ", total=" + total +
                ", date=" + date +
                '}';
    }
}
