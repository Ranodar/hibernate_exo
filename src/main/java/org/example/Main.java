package org.example;

import org.example.entities.Produit;
import org.example.services.ProduitService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {

        // Exercice 1

        // Creation des produits

//        ProduitService ps = new ProduitService();
//        ps.create(new Produit("TOSHIBA","zzaa123",new Date("2016/01/08"), 6000,555));
//        ps.create(new Produit("HP","EER678",new Date("2016/02/09"), 2000,444));
//        ps.create(new Produit("SONY VAIO","AQWZSX",new Date("2016/09/23"), 6000,333));
//        ps.create(new Produit("DELL","AZERTY",new Date("2016/02/12"), 6000,222));
//        ps.create(new Produit("SONY","qsdERT",new Date("2016/02/02"), 6000,111));

        // informations produit id = 2
//        Produit p = ps.findById(2);
//        System.out.println(p.getId()+ " , "+p.getMarque()+" , "+p.getReference());

        // supprimer le produit dont id = 3
//            ps.delete(ps.findById(3));

        // Modifier les informations du produit dont id =1
//        p = ps.findById(1);
//        if(p != null) {
//            p.setMarque("HP");
//            p.setReference("MMMMMPPPP");
//            p.setDateAchat(new Date("2015/09/08"));
//            p.setPrix(50000);
//            ps.update(p);
//        }
//
//
        /*
        String madate = "08/08/2022";
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse(madate);
        System.out.println(date);

        ProduitService ps = new ProduitService();
        List<Produit> produits = ps.findAll();
        for(Produit pr :  produits) {
            System.out.println(pr.getId());
        }

        double minPrice = 40000;
        List<Produit> produitspascher = null;
        try {
            produitspascher = ps.filterByPrice(minPrice);
            for(Produit pr :  produitspascher) {
                System.out.println(pr.getPrix());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        String madate1 = "01/01/2016";
        String madate2 = "01/01/2017";
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(madate1);
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(madate2);
        List<Produit> produitspardate = null;
        try {
            produitspardate = ps.filterByDate(date1, date2);
            for(Produit pr :  produitspardate) {
                System.out.println(pr.getId());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


         */

        /*
        ProduitService ps = new ProduitService();
        //Pour récupérer la saisie
        Scanner scanner = new Scanner(System.in);

        System.out.println("Merci de saisir le prix : ");
        double minPrice = scanner.nextDouble();
        List<Produit> produits = null;
        try {
            produits = ps.filterByPrice(minPrice);
            for(Produit pr :  produits) {
                System.out.println(pr.getId());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


        scanner.nextLine();
        System.out.println("Merci de saisir la date 1 (dd/MM/yyyy) : ");
        String s1= scanner.nextLine();
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(s1);

        System.out.println("Merci de saisir la date 2 (dd/MM/yyyy) : ");
        String s2= scanner.nextLine();
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(s2);
        //List<Produit> produits = null;
        try {
            produits = ps.filterByDate(date1, date2);
            for(Produit pr :  produits) {
                System.out.println(pr.getId());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
 */
            new Ihm().start();
    }


}