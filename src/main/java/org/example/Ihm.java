package org.example;

import net.bytebuddy.agent.builder.AgentBuilder;
import org.example.entities.Commande;
import org.example.entities.Comment;
import org.example.entities.Image;
import org.example.entities.Produit;
import org.example.services.ProduitService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Ihm {
    private ProduitService produitService;
    private Scanner scanner;

    public Ihm(){
        produitService = new ProduitService();
        scanner = new Scanner(System.in);
    }

    public void start() {
        String choice;
        do {
            menu();
            choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    valeurParMarque();
                    break;
                case "2":
                    moyenne();
                    break;
                case "3":
                    produitsMarques();
                    break;
                case "4":
                    deleteParMarque();
                    break;
                case "5":
                    ajouterImage();
                    break;
                case "6":
                    ajouterCommentaire();
                    break;
                case "7":
                    afficherProduitsAvecNote();
                    break;
                case "8":
                    creerCommande();
                    break;
                case "9":
                    afficherCommandes();
                    break;
                case "10":
                    afficherCommandesDuJour();
                    break;
                case "11":
                    break;
            }
        }while(!choice.equals("0"));
    }

    private void menu() {
        System.out.println("###########Menu###############");
        System.out.println("1 -- Afficher la valeur du stock par marque");
        System.out.println("2 -- prix moyen des produits");
        System.out.println("3 -- produits de plusieurs marques");
        System.out.println("4 -- supprimer produits par marque");
        System.out.println("5 -- Ajouter une image à un produit");
        System.out.println("6 -- Ajouter un commentaire à un produit");
        System.out.println("7 -- Afficher produit par note min");
        System.out.println("8 -- Créer une commande");
        System.out.println("9 -- Afficher toutes les commandes");
        System.out.println("10 -- Afficher toutes les commandes du jour");
        System.out.println("11 -- Ajouter une adresse de livraison");
    }

    private void valeurParMarque() {
        System.out.println("Merci de saisir la marque : ");
        String marque = scanner.nextLine();
        try {
            System.out.println("La valeur du stock est de : "+produitService.valeurStockParMarque(marque) + " euros ");
        }
        catch (Exception ex) {
            System.out.println("La valeur est de 0");
        }
    }

    private void moyenne() {
        try {
            System.out.println("prix moyen est de  : "+produitService.moyenne() + " euros ");
        }
        catch (Exception ex) {
            System.out.println("erreur calcule moyenne");
        }
    }

    private void produitsMarques(){
        List<String> marques = new ArrayList<>();
        marques.add("apple");
        marques.add("samsung");
        List<Produit> produits = null;
        try {
            produits = produitService.filterByMarques(marques);
            for(Produit pr : produits) {
                System.out.println(pr.getId());
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void deleteParMarque(){
        System.out.println("Merci de saisir la marque : ");
        String marque = scanner.nextLine();
        try {
            produitService.deleteByMarque(marque);
            System.out.println("Suppression Ok");
        }catch (Exception ex) {
            System.out.println("erreur suppression");
        }
    }

    private void ajouterImage() {
        System.out.println("Merci de saisir l'id du produit");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Merci de saisir l'url de l'image");
        String url = scanner.nextLine();
        Image image = new Image(url);
        if(produitService.addNewImage(image, id)) {
            System.out.println("Image ajoutée");
        }else {
            System.out.println("Erreur ajout d'image");
        }
    }

    private void ajouterCommentaire() {
        System.out.println("Merci de saisir l'id du produit ");
        int id = scanner.nextInt();
        scanner.nextLine();
        System.out.println("Merci de saisir le contenu du commentaire ");
        String content = scanner.nextLine();
        System.out.println("Merci de saisir la note :");
        int note = scanner.nextInt();
        scanner.nextLine();
        Comment comment = new Comment(content, note);
        if(produitService.addNewComment(comment, id)) {
            System.out.println("commentaire ajouté");
        }else {
            System.out.println("Erreur ajout de commentaire");
        }
    }

    private void afficherProduitsAvecNote() {
        System.out.println("Merci de saisir la note min");
        int note = scanner.nextInt();
        scanner.nextLine();
        List<Produit> produits = null;
        try {
            produits = produitService.getProduitsParNoteMin(note);
            for(Produit pr : produits) {
                System.out.println(pr.getId());
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void creerCommande() {
        StandardServiceRegistry registre = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Commande commande = new Commande();
        session.getTransaction().begin();
        System.out.println("Saisir le nombre de produits : ");
        int nb = scanner.nextInt();
        scanner.nextLine();
        double total = 0;
        for (int i=0;i<nb;i++){
            System.out.println("Saisir l'id du produit numéro "+(i+1));
            int id = scanner.nextInt();
            scanner.nextLine();
            Produit pr = produitService.findById(id);
            total += pr.getPrix();
            produitService.addProduitCommande(pr,commande.getId());
        }

        commande.setTotal(total);

        session.save(commande);
        session.getTransaction().commit();
    }

    private void afficherCommandes(){
        System.out.println("Liste des commandes du jour : ");
        List<Commande> commandes = produitService.findAllCommande();
        for(Commande c : commandes) {
            System.out.println(c.getId());
        }
    }

    private void afficherCommandesDuJour(){
        System.out.println("Liste des commandes du jour : ");
        List<Commande> commandes = produitService.findAllCommandeDuJour();
        for(Commande c : commandes) {
                System.out.println(c.getId());
        }
    }

}

