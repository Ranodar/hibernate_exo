package org.example.services;

import org.example.entities.Commande;
import org.example.entities.Comment;
import org.example.entities.Image;
import org.example.entities.Produit;
import org.example.interfaces.IDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class ProduitService implements IDAO<Produit> {

    private StandardServiceRegistry registre;

    private SessionFactory sessionFactory;

    private Session session;

    public ProduitService(){
        registre = new StandardServiceRegistryBuilder().configure().build();
        sessionFactory = new MetadataSources(registre).buildMetadata().buildSessionFactory();
        session = sessionFactory.openSession();
    }

    @Override
    public boolean create(Produit o) {
//        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean update(Produit o) {
//        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public boolean delete(Produit o) {
//        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.delete(o);
        session.getTransaction().commit();
        return true;
    }

    @Override
    public Produit findById(int id) {
        Produit produit = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        produit = (Produit)session.get(Produit.class, id);
        session.getTransaction().commit();
        return produit;
    }

    @Override
    public List<Produit> findAll() {
//        session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Produit> produitQuery= session.createQuery("from Produit");
        session.getTransaction().commit();
        return produitQuery.list();
    }

    @Override
    public List<Produit> filterByPrice(double min) throws Exception {
        if(min >= 0) {
//            session = sessionFactory.openSession();
            session.beginTransaction();
            Query<Produit> produitQuery = session.createQuery("from Produit where prix >= :min");
            produitQuery.setParameter("min",min);
            session.getTransaction().commit();
            return produitQuery.list();
        }
        throw new Exception("error value");
    }

    @Override
    public List<Produit> filterByDate(Date min, Date max) throws Exception {
        if(min.before(max)){
//            session = sessionFactory.openSession();
            session.beginTransaction();
            Query<Produit> produitQuery = session.createQuery("from Produit where dateAchat >= :min and dateAchat <= :max");
            produitQuery.setParameter("min",min);
            produitQuery.setParameter("max",max);
            session.getTransaction().commit();
            return produitQuery.list();
        }
        throw new Exception("error date");
    }

    public double valeurStockParMarque(String marque) {
//        session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Double> query = session.createQuery("select sum(prix) from Produit where marque =:marque");
        query.setParameter("marque",marque);
        return query.uniqueResult();
    }

    public double moyenne() {
//        session = sessionFactory.openSession();
        session.beginTransaction();
        Query<Double> query = session.createQuery("select avg(prix) from Produit");
        return query.uniqueResult();
    }

    public List<Produit> filterByMarques(List<String> marques) throws Exception {
        if(marques.size() > 0) {
//            session = sessionFactory.openSession();
            session.beginTransaction();
            Query<Produit> produitQuery= session.createQuery("from Produit where marque in :marques");
            produitQuery.setParameter("marques",marques);
            session.getTransaction().commit();
            return produitQuery.list();

        }
        throw new Exception("aucune marque");
    }

    public boolean deleteByMarque(String marque) {
//        session = sessionFactory.openSession();
        Query query = session.createQuery("delete Produit where marque = :marque");
        query.setParameter("marque",marque);
        session.getTransaction().begin();
        int success = query.executeUpdate();
        session.getTransaction().commit();
//        session.close();
        return success > 0;
    }

    // Ajouter une Image

    public boolean addNewImage(Image image, int id){
        boolean result = false;
        Produit produit = this.findById(id);
        session.getTransaction().begin();
        if(produit != null) {
            image.setProduit(produit);
            session.save(image);
            result = true;
        }
        session.getTransaction().commit();
        return result;
    }

    // Ajouter un commentaire
    public boolean addNewComment(Comment comment, int id){
        boolean result = false;
        Produit produit = this.findById(id);
        session.getTransaction().begin();
        if(produit != null) {
            comment.setProduit(produit);
            session.save(comment);
            result = true;
        }
        session.getTransaction().commit();
        return result;
    }

    // Recuperer un produit par note
    public List<Produit> getProduitsParNoteMin(int note){
//        session = sessionFactory.openSession();
        Query<Produit> produitQuery= session.createQuery("select distinct produit from Comment where note >=:note");
        produitQuery.setParameter("note",note);
        return produitQuery.list();
    }

    // findCommandeById
    public Commande findCommandeById(int id) {
        Commande commande = null;
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        commande = (Commande) session.get(Commande.class, id);
        session.getTransaction().commit();
        return commande;
    }

    public List<Commande> findAllCommande() {
            session.beginTransaction();
            Query<Commande> commandeQuery= session.createQuery("from Commande");
            session.getTransaction().commit();
            return commandeQuery.list();
        }

    public List<Commande> findAllCommandeDuJour() {
        LocalDate maDate = LocalDate.now();
        session.beginTransaction();
        Query<Commande> commandeQuery= session.createQuery("from Commande where date =:madate");
        commandeQuery.setParameter("madate",maDate);
        session.getTransaction().commit();
        return commandeQuery.list();
    }

    // Ajouter un produit à une commande
    public boolean addProduitCommande(Produit pr, int id){
        boolean result = false;
        Commande commande = this.findCommandeById(id);
        if(commande != null) {
            commande.ajouterProduits(pr);
            result = true;
        }
        return result;
    }




}
